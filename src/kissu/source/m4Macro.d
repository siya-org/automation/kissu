struct macroName{
  string name;
}

struct userDefinedField{}

string getM4Macro(T)(const T obj) {
  import  std.conv: to;
  import std.format;
  import std.traits;
  import std.typecons;
  import std.string: strip;
  string output;
  static foreach(m; [__traits(allMembers, T)])
    static if (hasUDA!(__traits(getMember, T, m), macroName)){
      static if (hasUDA!(__traits(getMember, T, m), userDefinedField)) {
	output ~= getM4Macro!(typeof(__traits(getMember, T, m)))( mixin("obj." ~ m));
      } else {
	output ~= format("define(%s, %s)dnl\n",
			 getUDAs!(__traits(getMember, T, m),
				  macroName)[0].name,
			 mixin("obj."~m));
      }
    }
  // remove any newlines around the M4 defs
  return output.strip("\n");
}
