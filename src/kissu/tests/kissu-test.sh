. ./assert.sh			# source assert

#########################
# TEST CLIENT
#########################

# TEST CLIENT CREATE
assert_raises "$1 client create " 4
assert_raises "$1 client create -u ayushjha" 4
assert_raises "$1 client create -u ayushjha -n 'Ayush Jha'" 0
assert_raises "$1 client create -u ayushjha -n 'Ayush Jha'" 2
assert_end "client create"

# TEST CLIENT GET
assert_raises "$1 client get -u ramb" 4
assert_raises "$1 client get -u ayushjha" 0
assert_end "client get"

# TEST CLIENT UPDATE
assert_raises "$1 client update " 4
assert_raises "$1 client update -u ayushjha" 4
assert_raises "$1 client update -u ayushjha -n 'Ayu Jha'" 0
assert_raises "$1 client update -u idontexist -n 'Ayush Jha'" 3
assert_end "client update"

# TEST CLIENT DELETE
assert_raises "$1 client delete -u idonotexist" 4
assert_raises "$1 client delete -u ayushjha" 0
assert_end "client delete"

#########################
# END OF CLIENT TESTS
#########################

#########################
# TEST SERVICE
#########################
$1 client create -u ayushjha -n 'Ayush Jha'
# TEST SERVICE CREATE
assert_raises "$1 service create -u ayushjha -n moodle -p 1000" 0
assert_raises "$1 service create -u ayushjha -n odoo -p 1000" 7

assert_raises "$1 service create -u ayushjha -n odoo -p 1001" 0
assert_raises "$1 service create -u ayushjha -n odoo -p 1001" 7
assert_end "service create"

# TEST SERVICE GET
assert_raises "$1 service get -u ramb -n moodle" 9
assert_raises "$1 service get -u ayushjha -n moodle" 0
assert_raises "$1 service get -u ayushjha -n doesnotexist" 9
assert_end "service get"

# TEST SERVICE UPDATE
assert_raises "$1 service update -u ayushjha -n moodle -p 9987" 0
assert_raises "$1 service update -u ayushjha -n moodle -p 1000" 0
assert_raises "$1 service update -u ayushjha -n odoo -p 1001" 0
assert_raises "$1 service update -u ayushjha -n odoo -p 9900" 0
assert_raises "$1 service update -u ayushjha -n doesntexist -p 1001" 8
assert_raises "$1 service update -u ramb -n moodle -p 1001" 6
assert_end "service update"

# TEST SERVICE DELETE
assert_raises "$1 service delete -u ramb -n moodle" 9
assert_raises "$1 service delete -u ayushjha -n moodle" 0
assert_raises "$1 service delete -u ayushjha -n doesnotexist" 9
assert_end "service delete"

#########################
# END OF SERVICE TESTS
#########################
